#!/usr/bin/env python2.7

import sys

def build_tm(input_file):
	tm = dict()
	for line in input_file:
		print(line.rstrip())
	return tm

def run_tm(tm, tape_file):
	pass

def main(argv):
	## Usage
	if len(argv) != 3:
		print("Usage: tm-qjg.py MACHINE_FILE TAPE_FILE")
		return
	## Build TM
	tm = build_tm(argv[1])
	## Run TM
	run_tm(tm, argv[2])
	

if __name__ == "__main__":
	main(sys.argv)